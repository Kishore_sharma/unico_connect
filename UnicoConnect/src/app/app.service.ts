import { Injectable } from '@angular/core';
import { throwError } from "rxjs";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) {
    this.getHttpHeader();
  }

  getHttpHeader () {
      const headers = {
        headers: new HttpHeaders({
          "X-Requested-With": "Test key",
          "X-location": "none",
        }).set("Authorization", `Bearer ${localStorage.getItem("token") || null}`),
      };
      return headers;
  }

  handleError(errorResponse: HttpErrorResponse){
    if(errorResponse.error instanceof ErrorEvent){
      console.error('Client Side Error', errorResponse.error.message);
    } else {
      console.error("Server side error", errorResponse);
      alert(errorResponse.error.error);
    }

    return throwError('There is a problem with the service. Try Again Later');
  }
  
}
