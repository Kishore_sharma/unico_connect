import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor( private http : HttpService) { }

  onLoginSubmit = (data : any) => {
    return this.http.post(`/v1/login/`, data);
  }

}
