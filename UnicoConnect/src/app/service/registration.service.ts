import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http : HttpService) { }

  onRegister = (data : any) => {
    return this.http.post(`/v1/registration/`, data);
  }
}
