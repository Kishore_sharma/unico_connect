import { Injectable } from '@angular/core';
import { catchError } from "rxjs/operators";
import { AppService } from '../app.service';
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private headers: any;

  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  createHeader = () => this.appService.getHttpHeader();

  post = (url : string, body: any) => {
    return this.http.post<any>(`${environment.SERVER_API}${url}`, body, this.createHeader()).pipe(catchError(this.appService.handleError));
  }

  get = (url : string) => {
    return this.http.get<any>(`${environment.SERVER_API}${url}`, this.createHeader()).pipe(catchError(this.appService.handleError));
  }

  put = (url : string, body: any) => {
    return this.http.put<any>(`${environment.SERVER_API}${url}`, body, this.createHeader()).pipe(catchError(this.appService.handleError));
  }

  delete = (url : string) => {
    return this.http.delete<any>(`${environment.SERVER_API}${url}`, this.createHeader()).pipe(catchError(this.appService.handleError));
  }
  
}
