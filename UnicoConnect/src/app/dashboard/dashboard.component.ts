import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../service/dashboard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  stuDetails : any;
  stuPrevDetails : any = null;
  fetching : boolean = true;

  constructor(private dashboard : DashboardService, private router : Router) {
    this.dashboard.getAllDetail().subscribe((data : any) => {
      if(data.success) {
        this.stuDetails = data.data[0];
        this.stuPrevDetails = data.data;
        this.fetching = false;
      }
    })
  }

  ngOnInit(): void {
  }

  logout = () => {
    localStorage.clear();
    this.router.navigate(['']);
  }

}
