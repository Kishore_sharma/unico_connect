import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;

  formErrors = {
    'email': '',
    'password': '',
  };

  validationMessages = {
    'email': {
      'required':      'Email is required.',
      'minlength':     'Email is too short',
      'maxlength':     'Email is too long',
      'pattern' :      'Invalid Email'
    },
    'password': {
      'required':      'Password is required.',
      'minlength':     'Password is too short',
      'maxlength':     'Password is too long',
    },
  };

  constructor(
    private fb: FormBuilder,
    private loginService : LoginService,
    private router : Router,
  ) { 
    this.createLoginForm();
  }

  ngOnInit(): void {
  }

  createLoginForm = () => {
    this.loginForm = this.fb.group({
      email : [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      password : [null, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]]
    });

    this.loginForm.valueChanges.subscribe((data : any) => this.onLoginValueChanged(data));

    this.onLoginValueChanged(); // (re)set validation messages now

  }

  onLoginValueChanged(data?: any) {
    if (!this.loginForm) { return; }
    const form = this.loginForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ' + '\n';
            }
          }
        }
      }
    }
  }

  onLoginSubmit = () => {
    this.loginService.onLoginSubmit(this.loginForm.value).subscribe((data : any) => {
      if(data.success) {
        localStorage.setItem("token", data.token);
        this.loginForm.reset();
        this.router.navigate(['/dashboard']);
      }
      else alert("Something goes wrong");
    });
  }

}
