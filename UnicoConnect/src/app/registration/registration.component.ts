import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegistrationService } from '../service/registration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  passwordSame: boolean = false;

  formErrors = {
    name: '',
    rollNumber: '',
    email: '',
    password: '',
    currentStandard: '',
  };

  validationMessages = {
    name: {
      required: 'Name is required',
      pattern: 'Invalid name',
      minlength: 'Name is too short',
      maxlength: 'Name is too long',
    },
    rollNumber: {
      required: 'Roll Number is required',
      minlength: 'Roll Number is too short',
      maxlength: 'Roll Number is too long',
    },
    email: {
      required: 'Email is required.',
      minlength: 'Email is too short',
      maxlength: 'Email is too long',
      pattern: 'Invalid Email',
    },
    password: {
      required: 'Password is required.',
      minlength: 'Password is too short',
      maxlength: 'Password is too long',
    },
    currentStandard: {
      required: 'Current Standard is required',
    },
    cpassword : {
      mismatch : 'Password and confirm password not matched',
    },
    studentMarks: {
      remarks: {
        required: 'Remarks is required.',
        minlength: 'Remarks is too short',
        maxlength: 'Remarks is too long',
        pattern: 'Invalid Remarks',
      },
      percentage: {
        required: 'Percentage is required.',
        pattern: 'Invalid Percentage',
      },
    },
  };

  Standards = [
    { label: '1st' },
    { label: '2nd' },
    { label: '3rd' },
    { label: '4th' },
    { label: '5th' },
    { label: '6th' },
    { label: '7th' },
    { label: '8th' },
    { label: '9th' },
  ];

  constructor(
    private fb: FormBuilder,
    private regService: RegistrationService,
    private router : Router,
  ) {
    this.createRegistrationForm();
  }

  ngOnInit(): void {}

  createRegistrationForm = () => {
    this.registerForm = this.fb.group(
      {
        name: [
          null,
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(100),
            Validators.pattern(/^[a-zA-Z ]+$/),
          ],
        ],
        rollNumber: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
        email: [
          null,
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
          ],
        ],
        password: [
          null,
          [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(20),
          ],
        ],
        cpassword: [null],
        currentStandard: [null, [Validators.required]],
        studentMarks: new FormArray([]),
      },
      {
        validators: this.confirmPassword.bind(this),
      }
    );

    this.registerForm.valueChanges.subscribe((data: any) =>
      this.onValueChanged(data)
    );

    this.onValueChanged();
  };

  get studentMark() {
    return this.registerForm.controls.studentMarks as FormArray;
  }

  onValueChanged(data?: any) {
    if (!this.registerForm) {
      return;
    }
    const form = this.registerForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ' + '\n';
            }
          }
        }
      }
    }
  }

  confirmPassword = (formGroup: FormGroup) => {
    return (this.passwordSame =
      formGroup.get('password').value === formGroup.get('cpassword').value
        ? true
        : false);
  };

  onRegister = () => {
    console.log(this.registerForm.value);
    this.regService
      .onRegister(this.registerForm.value)
      .subscribe((data: any) => {
        if (data.success) alert('Successfully Registered');
        this.registerForm.reset();
        this.router.navigate(['']);
      });
  };

  changeStandard = () => {
    let currentStandard = this.registerForm.get('currentStandard').value;
    if (currentStandard == '1st') return;
    let length = Number(currentStandard.slice(0, 1) - 1);
    for (let i = this.studentMark.length; i < length; i++) {
      this.studentMark.push(
        this.fb.group({
          standard: [this.Standards[i].label],
          remarks: [
            null,
            [
              Validators.required,
              Validators.pattern(/^[a-zA-Z ]+$/),
              Validators.minLength(2),
              Validators.maxLength(10),
            ],
          ],
          percentage: [
            null,
            [
              Validators.required,
              Validators.pattern(/^([\d]{0,5})(\.[\d]{1,2})?$/),
            ],
          ],
        })
      );
    }
  };
}
