const ErrorResponse = require("../../utils/errorResponse");
const { asyncHandler } = require("../../middleware/async");
const { Dashboard } = require("../../models/Dashboard/Dashboard");
const { INTERNAL_SERVER_ERROR } = require("../../utils/errResponseMessage");
const _ = require("underscore");

//@desc     GET Dashboard
//@route    GET /api/v1/dashboard/
//@access   Private
exports.dashboard = asyncHandler(async (req, res, next) => {

    const { err, rows } = await Dashboard.getStudentDetails(req.user);

    if(err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    res.status(200).json({
        success: true,
        data : rows
    }).end();
    
});

