const ErrorResponse = require("../../utils/errorResponse");
const { asyncHandler } = require("../../middleware/async");
const { Student } = require("../../models/Student/Student");
const { INTERNAL_SERVER_ERROR } = require("../../utils/errResponseMessage");
const _ = require("underscore");

//@desc     GET All Student
//@route    GET /api/v1/students/
//@access   Public
exports.allStudentRecords = asyncHandler(async (req, res, next) => {

    const { err, rows } = await Student.getAllRecords();

    if(err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    res.status(200).json({
        success: true,
        data : rows
    }).end();
    
});

