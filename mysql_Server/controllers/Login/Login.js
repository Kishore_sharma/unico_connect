const ErrorResponse = require("../../utils/errorResponse");
const { asyncHandler } = require("../../middleware/async");
const { Login } = require("../../models/Login/Login");
const { LoginValidation } = require("../../validation/Login");
const { INTERNAL_SERVER_ERROR, INVALID_CREDENTIALS } = require("../../utils/errResponseMessage");
const _ = require("underscore");
const jwt = require("jsonwebtoken");

//@desc     POST Login
//@route    POST /api/v1/login/
//@access   Public
exports.login = asyncHandler(async (req, res, next) => {

    const { error } = LoginValidation(req.body);

    if(error) return next(new ErrorResponse(error.details[0].message, 400));

    const { err, rows } = await Login.login(req.body);

    if(err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    if(_.isEmpty(rows)) return next(new ErrorResponse(INVALID_CREDENTIALS, 401));

    sendTokenResponse(rows, 200, res);
    
});

const sendTokenResponse = (rows, statusCode, res) => {

    const token = jwt.sign({ id : rows[0].ROLL_NUMBER }, process.env.JWT_SECRET, { expiresIn : process.env.JWT_EXPIRE });

    const options = {
        expires : new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000),
        httpOnly : true,
    };

    res.status(statusCode).cookie('token', token, options).json({ success : true, token });
}

