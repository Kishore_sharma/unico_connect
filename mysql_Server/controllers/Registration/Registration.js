const ErrorResponse = require("../../utils/errorResponse");
const { asyncHandler } = require("../../middleware/async");
const { Registration } = require("../../models/Registration/Registration");
const { RegistrationValidation } = require("../../validation/Registration");
const { INTERNAL_SERVER_ERROR, ROLL_NO_ALREADY_REGISTERED } = require("../../utils/errResponseMessage");
const _ = require("underscore");

//@desc     POST Save Registration
//@route    POST /api/v1/registration/
//@access   Public
exports.register = asyncHandler(async (req, res, next) => {

    const { err : err1 , rows } = await Registration.checkRollNo(req.body.rollNumber);

    if(err1) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    if(rows?.COUNT) return next(new ErrorResponse(ROLL_NO_ALREADY_REGISTERED, 409))

    if(_.isEmpty(req.body.studentMarks)) req.body.studentMarks = {};

    const { error } = RegistrationValidation(req.body);

    if(error) return next(new ErrorResponse(error.details[0].message, 400));

    const { err } = await Registration.saveRegisteration(req.body);

    if (err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    console.log(req.body?.currentStandard, req.body?.currentStandard !== "1st");

    if(req.body?.currentStandard !== "1st") {

      const { err } = await Registration.savePreviousStandards(req.body?.studentMarks, req.body?.rollNumber);

      if(err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

    }

    res.status(201).json({
      success: true,
      data: "Successfully Registered"
    }).end();

});

