const jwt = require("jsonwebtoken");
const { asyncHandler } = require("./async");
const ErrorResponse = require("../utils/errorResponse");
const { Login } = require("../models/Login/Login");
const { NOT_AUTHORIZE_TO_ACCESS_ROUTE, INTERNAL_SERVER_ERROR, RESOURCE_NOT_FOUND } = require("../utils/errResponseMessage");
const _ = require("underscore");


//Protect Route
exports.authProtect = asyncHandler(async (req, res, next) => {

    let token;

    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')) 
        token = req.headers.authorization.split(' ')[1];
    
    if(!token) return next(new ErrorResponse(NOT_AUTHORIZE_TO_ACCESS_ROUTE, 401));

    try {
        //Verify Token
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        const { err, rows } = await Login.checkStudentAvailability(decoded.id);

        if(err) return next(new ErrorResponse(INTERNAL_SERVER_ERROR, 500));

        if(_.isEmpty(rows)) return next(new ErrorResponse(RESOURCE_NOT_FOUND, 400));

        req.user = rows?.ROLL_NUMBER;

        next();

    } catch (err) {
        return next(new ErrorResponse(NOT_AUTHORIZE_TO_ACCESS_ROUTE, 401));
    }

});