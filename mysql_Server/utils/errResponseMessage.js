const message = {
    INTERNAL_SERVER_ERROR: "Internal Server Error",
    ROLL_NO_ALREADY_REGISTERED : "Roll Number Already Registered",
    INVALID_CREDENTIALS : "Invalid Credentials",
    NOT_AUTHORIZE_TO_ACCESS_ROUTE : "Not authorize to access this route",
    RESOURCE_NOT_FOUND : "Resource Not Found"
}

module.exports = message;