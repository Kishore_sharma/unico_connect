const mysql = require('mysql');
const db_config = require('./database.json');
const pool = mysql.createPool(db_config[process.env.NODE_ENV]);

exports.database = () => {
    return {
        select: (query, callback) => {
            pool.getConnection((err, connection) => {
                if (err) {
                    console.log('Error connecting to mysql');
                    throw err;
                }
                connection.query(query, (err, rows, fields) => {
                    connection.release();
                    callback(err, rows);
                });
            });
        },
        selectParam: (query, param, callback) => {
            pool.getConnection((err, connection) => {
                if (err) {
                    console.log('Error connecting to mysql');
                    throw err;
                }
                connection.query(query, param, (err, rows, fields) => {
                    connection.release();
                    callback(err, rows);
                });
            });
        }
    }   
}