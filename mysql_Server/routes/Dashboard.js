const express = require("express");
const { dashboard } = require("../controllers/Dashboard/Dashboard");
const { authProtect } = require("../middleware/auth");

const router = express.Router();

router.route('/').get(authProtect, dashboard);

module.exports = router;