const express = require("express");
const { login } = require("../controllers/Login/Login");

const router = express.Router();

router.route('/').post(login);

module.exports = router;