const express = require("express");
const { register } = require("../controllers/Registration/Registration");

const router = express.Router();

router.route('/').post(register);

module.exports = router;