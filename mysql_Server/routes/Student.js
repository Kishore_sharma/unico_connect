const express = require("express");
const { allStudentRecords } = require("../controllers/Student/Student");

const router = express.Router();

router.route('/').get(allStudentRecords);

module.exports = router;