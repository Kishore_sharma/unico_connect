const faker = require("faker");
const dotenv = require("dotenv");
dotenv.config({ path: './config/config.env' });
const database = require("./config/database").database();

const insertData = () => {
    const query = `INSERT INTO STUDENT_RECORDS(ID, NAME, EMAIL, PASSWORD, DOB, PHONE_NO, COUNTRY, ADDRESS, AVATAR) VALUES ?`;
    let params = [];
    params.push(faker.datatype.uuid());
    params.push(faker.name.findName());
    params.push(faker.internet.email());
    params.push(faker.internet.password());
    params.push(faker.date.past());
    params.push(faker.phone.phoneNumberFormat());
    params.push(faker.address.country());
    params.push(`${faker.address.streetAddress()} ${faker.address.city()}, ${faker.address.state()}`);
    params.push(faker.image.avatar());

    database.selectParam(query, [[ params ]], (err, rows) => console.log(rows));
}

for(let i = 0; i < 20000; i++) {
    insertData();
}