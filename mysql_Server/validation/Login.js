const Joi = require("joi");

exports.LoginValidation = data => {

    const schema = Joi.object({
        email: Joi.string().min(3).max(50).regex(new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)).email()
            .required().trim().messages({
                "string.base": `Email should be a type of string`,
                "string.empty": `Email must contain value`,
                "string.pattern.base": `Email is invalid`,
                "any.required": `Email is required`,
                "string.min": 'Email should contain atleast 3 letters/digits',
                "string.max": 'Email should not contain more than 50 letters/digits'
            }),
        password: Joi.string().min(5).max(20).required().trim().messages({
            "string.base": `Password should be a type of string`,
            "string.empty": `Password must contain value`,
            "any.required": `Password is required`,
            "string.min": 'Password should contain atleast 5 letters/digits',
            "string.max": 'Password should not contain more than 20 letters/digits'
        }),
    });

    return schema.validate(data);

}