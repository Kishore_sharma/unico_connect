const Joi = require("joi");

exports.RegistrationValidation = data => {

    const schema = Joi.object({
        rollNumber: Joi.string().alphanum().min(3).max(10).required().trim().messages({
            "string.base": `Roll Number should be a type of string`,
            "string.empty": `Roll Number must contain value`,
            "any.required": `Roll Number is required`,
            "string.min": 'Roll Number should contain atleast 3 letters/digits',
            "string.max": 'Roll Number should not contain more than 10 letters/digits'
        }),
        name: Joi.string().min(2).max(100).required().trim().regex(new RegExp(/^[a-zA-Z ]+$/)).messages({
            "string.base": `Name should be a type of string`,
            "string.empty": `Name must contain value`,
            "string.pattern.base": `Name is invalid`,
            "any.required": `Name is required`,
            "string.min": 'Email should contain atleast 2 letters/digits',
            "string.max": 'Email should not contain more than 100 letters/digits'
        }),
        email: Joi.string().min(3).max(50).regex(new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)).email().required().trim().messages({
            "string.base": `Email should be a type of string`,
            "string.empty": `Email must contain value`,
            "string.pattern.base": `Email is invalid`,
            "any.required": `Email is required`,
            "string.min": 'Email should contain atleast 3 letters/digits',
            "string.max": 'Email should not contain more than 50 letters/digits'
        }),
        password: Joi.string().min(5).max(20).required().trim().messages({
            "string.base": `Password should be a type of string`,
            "string.empty": `Password must contain value`,
            "any.required": `Password is required`,
            "string.min": 'Password should contain atleast 5 letters/digits',
            "string.max": 'Password should not contain more than 20 letters/digits'
        }),
        cpassword: Joi.string().min(5).max(20).required().trim().equal(Joi.ref('password')).messages({
            "string.base": `Confirm Password should be a type of string`,
            "string.empty": `Confirm Password must contain value`,
            "any.required": `Confirm Password is required`,
            "string.min": 'Confirm Password should contain atleast 5 letters/digits',
            "string.max": 'Confirm Password should not contain more than 20 letters/digits'
        }),
        currentStandard: Joi.string().valid('1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th').required().trim().label("Current Standard").messages({
            "string.base": `Current Standard should be a type of string`,
            "string.empty": `Current Standard must contain value`,
            "any.required": `Current Standard is required`,
            "any.only" : `Invalid Standard`
        }),
        studentMarks: Joi.when('currentStandard', {
            is: "1st",
            then: {},
            otherwise: Joi.array().min(Number(data.currentStandard.slice(0, 1)) - 1).max((Number(data.currentStandard.slice(0, 1)) - 1)).items(Joi.object({
                standard : Joi.string().valid('1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th').required().trim().messages({
                    "string.base": `Standard should be a type of string`,
                    "string.empty": `Standard must contain value`,
                    "any.required": `Standard is required`,
                    "any.only" : `Invalid Standard`
                }),
                remarks: Joi.string().min(2).max(10).required().trim().regex(new RegExp(/^[a-zA-Z ]+$/)).messages({
                    "string.base": `Remarks should be a type of string`,
                    "string.empty": `Remarks must contain value`,
                    "any.required": `Remarks is required`,
                    "string.pattern.base": `Remarks is invalid`,
                    "string.min": 'Remarks should contain atleast 2 letters/digits',
                    "string.max": 'Remarks should not contain more than 10 letters/digits'
                }),
                percentage: Joi.number().required().messages({
                    "number.base": `Percentage should be a type of number`,
                    "number.empty": `Percentage must contain value`,
                    "any.required": `Percentage is required`,
                })
            }))
        }).messages({
            "array.min": `Student Marks should contain atleast ${Number(data.currentStandard.slice(0, 1) - 1)} items`,
            "array.max": `Student Marks should not contain more than ${Number(data.currentStandard.slice(0, 1) - 1)} items`
        })
    });

    return schema.validate(data);

}