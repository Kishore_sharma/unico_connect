Create a Database with name "unico_connect"

Import the sql file which is inside the mysql_Server/config/ folder.

Check the database.json file which is inside the mysql_Server/config/ folder for configuration of userid and password for mysql.

Run "npm i" inside mysql_Server and also inside UnicoConnect Folder to get all the dependencies.

Run "npm start" inside mysql_Server to start the Server. If everything is OK it will show :- 

    "Server is running in DEV mode on port 8080"

Backend Server is UP.

Now For Front End You have to go inside UnicoConnect/ and then run ng serve -o

