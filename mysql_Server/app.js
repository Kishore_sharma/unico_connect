// load our app server
const express = require('express');
const app = express();
const dotenv = require("dotenv");
const morgan = require('morgan');
const _ = require("underscore");

dotenv.config({ path: './config/config.env' });

//Importing routes
const login = require("./routes/Login");
const registration = require("./routes/Registration");
const dashboard = require("./routes/Dashboard");
const students = require("./routes/Student");

const cors = require("cors");
const errorHandler = require("./middleware/error");

//CORS middleware
const corsOptions = {
  origin: ["http://localhost:4200"],
  allowedHeaders: [
    "headers",
    "Access-Control-Allow-Headers",
    "Origin,  X-Requested-With, X-location, Content-Type, Accept, Cache-Control, Authorization",
  ],
  methods: "GET,POST,OPTIONS,DELETE,PUT",
  preflightContinue: false,
  optionsSuccessStatus: 200,
};


app.options("*", cors(corsOptions)); // include before other routes
app.use(cors(corsOptions)); // include before other routes

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({extended: true}));

// parse application/json
app.use(express.json());

app.use(morgan('short'));

//Mounting Routers 
app.use("/api/v1/students", students);

app.use("/api/v1/login", login);

app.use("/api/v1/registration", registration);

app.use("/api/v1/dashboard", dashboard);

app.use(errorHandler);


const PORT = process.env.PORT || 8080;

const server = app.listen(PORT, () => {
  console.log(`Server is running in ${process.env.NODE_ENV} mode on port ${PORT}`);
});

//Handle unhandled promise rejected
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error : ${err.message}`);
  //Close server & exit process
  server.close((error) => process.exit(1));
});