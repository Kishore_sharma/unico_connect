const database = require("../../config/database").database();

exports.Login = {

  login : (data) => {
    return new Promise((resolve) => {
        const query = `SELECT ROLL_NUMBER FROM STUDENT_REGISTRATION WHERE PASSWORD = ? AND EMAIL = ?`;
        database.selectParam(query, [data.password, data.email], (err, rows) => resolve({ err,rows }));
    })
  },

  checkStudentAvailability : (rollNumber) => {
    return new Promise((resolve) => {
      const query = `SELECT COUNT(*) AS COUNT, ROLL_NUMBER FROM STUDENT_REGISTRATION WHERE ROLL_NUMBER = ?`;
      database.selectParam(query, [rollNumber], (err, rows) => resolve({ err, rows : rows[0]}));
    })
  }
  
}