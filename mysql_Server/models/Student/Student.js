const database = require("../../config/database").database();

exports.Student = {

  getAllRecords : () => {
    return new Promise((resolve) => {
        const query = `SELECT * FROM STUDENT_RECORDS`;
        database.select(query, (err, rows) => resolve({ err,rows }));
    })
  }
  
}