const database = require("../../config/database").database();

exports.Dashboard = {

  getStudentDetails : (rollNumber) => {
    return new Promise((resolve) => {
        const query = `SELECT ROLL_NUMBER,NAME,CURRENT_STANDARD,STANDARD, REMARKS, PERCENTAGE FROM STUDENT_REGISTRATION LEFT JOIN 
        STUDENT_PREVIOUS_STANDARDS ON ROLL_NUMBER = STUDENT_ROLL_NUMBER WHERE ROLL_NUMBER = ?`;
        
        database.selectParam(query, [rollNumber], (err, rows) => resolve({ err,rows }));
    })
  }
  
}