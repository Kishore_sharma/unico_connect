const database = require("../../config/database").database();

exports.Registration = {

    checkRollNo : (rollNumber) => {
        return new Promise((resolve) => {
            const query = `SELECT COUNT(*) AS COUNT FROM STUDENT_REGISTRATION WHERE ROLL_NUMBER = ?`;
            database.selectParam(query, [rollNumber], (err, rows) => resolve({ err, rows : rows[0] }))
        })
    },

    saveRegisteration: (data) => {
        return new Promise((resolve) => {
            const query = `INSERT INTO STUDENT_REGISTRATION(ROLL_NUMBER,NAME,EMAIL,PASSWORD,CURRENT_STANDARD) VALUES ?`;
            let params = [];
            params.push(data.rollNumber);
            params.push(data.name);
            params.push(data.email);
            params.push(data.password);
            params.push(data.currentStandard);
            database.selectParam(query, [
                [params]
            ], (err, rows) => resolve({
                err,
                rows
            }));
        })
    },

    savePreviousStandards: (standardMark, rollNumber) => {
        return new Promise((resolve) => {
            const query = `INSERT INTO STUDENT_PREVIOUS_STANDARDS(STUDENT_ROLL_NUMBER, STANDARD, REMARKS, PERCENTAGE) VALUES ?`;
            database.selectParam(query, [standardMark.map(item => [rollNumber, item.standard, item.remarks, item.percentage])], (err, rows) => resolve({
                err,
                rows
            }))
        })
    }
}